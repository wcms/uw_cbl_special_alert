<?php

/**
 * @file
 * Contains \Drupal\uw_cbl_special_alert\Plugin\Block\SpecialAlertBlock.
 */
namespace Drupal\uw_cbl_special_alert\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
/**
 *  Provides a 'Special Alert Test' block.
 *
 *  @Block(
 *    id = "uw_cbl_special_alert",
 *    admin_label = @Translation("Special Alert"),
 *  )
 */
class SpecialAlertBlock extends BlockBase {
  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'uw_cbl_special_alert.settings';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'display_option' => FALSE,
        'message' => [],
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $build = [
      '#markup' => $config['message']['value'] ?? '',
    ];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    $form['display_option'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display Alert'),
      '#default_value' => $config['display_option'],
      '#weight' => '0',
    ];
    $form['message'] = [
      '#type' => 'text_format',
      '#format' => 'uw_tf_basic',
      '#allowed_formats' => ['uw_tf_basic'],
      '#title' => $this->t('Special Alert Message'),
      '#required' => TRUE,
      '#default_value' => $config['message']['value'] ?? '',
      '#weight' => '1',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    $config['display_option'] = $form_state->getValue('display_option');
    $config['message'] = $form_state->getValue('message');
    $this->setConfiguration($config);
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    $config = $this->getConfiguration();
    $display = $config['display_option'];
    $hasPermission = AccessResult::allowedIfHasPermission($account, 'access content')->isAllowed();
    if ($hasPermission && $display) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }
}
