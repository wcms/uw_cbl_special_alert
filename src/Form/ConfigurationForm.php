<?php
/**
 * @file
 * Contains \Drupal\uw_cbl_special_alert\Form\ConfigurationForm.
 */
namespace Drupal\uw_cbl_special_alert\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfigurationForm.
 */
class ConfigurationForm extends ConfigFormBase {
  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /** @var \Drupal\block\BlockInterface */
  protected $block;

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'uw_cbl_special_alert.settings';

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdocs}
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entityTypeManager;

    $storage = $this->entityTypeManager->getStorage('block');
    $all_blocks = $storage->loadByProperties([
      'plugin' => 'uw_cbl_special_alert',
    ]);

    // Block id is specialalert and I do not know why.
    //$this->block = $all_blocks['uw_cbl_special_alert'];
    // Grab the first block.  If you have multiple then only the first one will
    // get changed.
    $this->block = array_pop($all_blocks);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ub_cbl_special_alert_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    if (!$this->block) {
      $form['message'] = [
        '#markup' => t('You need to place the block first before you configure it.'),
      ];
      return $form;
    }

    $values = $this->block->getVisibility();
    $settings = $this->block->get('settings');

    $form['display_option'] = [
      '#type' => 'checkbox',
      '#title' => t('Display This Alert'),
      '#default_value' => $settings['display_option'],
    ];
    $form['message'] = [
      '#type' => 'text_format',
      '#format' => 'uw_tf_basic',
      '#allowed_formats' => ['uw_tf_basic'],
      '#title' => t('Special Alert Message'),
      '#required' => TRUE,
      '#default_value' => $settings['message']['value'] ?? '',
    ];
    $form['pages'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Pages'),
      '#default_value' => $values['request_path']['pages'] ?? '',
    ];

    $form['request_path']['#title'] = $this->t('Pages');
    $form['request_path']['negate']['#type'] = 'radios';
    $form['request_path']['negate']['#default_value'] = (int) $values['request_path']['negate'];
    $form['request_path']['negate']['#title_display'] = 'invisible';
    $form['request_path']['negate']['#options'] = [
      $this->t('Show for the listed pages'),
      $this->t('Hide for the listed pages'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save the visibility and extra settings (display and message).
    $form_state->cleanValues();
    $settings = $this->block->get('settings');
    $vis = $this->block->getVisibility();
    // Have to set the request_path the first time, or saving on the form will
    // throw a php error that it cannot find it when you set the value.
    if (count($vis) <= 0) {
      $this->block->setVisibilityConfig('request_path', []);
    }
    $vis['request_path']['pages'] = $form_state->getValue('pages');
    $vis['request_path']['negate'] = $form_state->getValue('negate');
    $settings['display_option'] = $form_state->getValue('display_option');
    $settings['message'] = $form_state->getValue('message');
    $this->block->set('visibility', $vis);
    $this->block->set('settings', $settings);
    $this->block->save();

    parent::submitForm($form, $form_state);
  }
}
